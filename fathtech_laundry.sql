-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 08, 2019 at 03:25 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fathtech_laundry`
--

-- --------------------------------------------------------

--
-- Table structure for table `ak_data_customer`
--

CREATE TABLE `ak_data_customer` (
  `id_customer` int(10) NOT NULL,
  `nama_customer` varchar(35) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `tanggal_dibuat` datetime NOT NULL,
  `tanggal_dirubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userchange` text NOT NULL,
  `id_outlet` int(10) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ak_data_karyawan`
--

CREATE TABLE `ak_data_karyawan` (
  `id_karyawan` bigint(15) NOT NULL,
  `nik` int(20) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `username` varchar(35) NOT NULL,
  `password` text NOT NULL,
  `id_level` int(10) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` text NOT NULL,
  `tanggal_dibuat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_dirubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userchange` text NOT NULL,
  `id_outlet` int(11) NOT NULL,
  `id_pengaturan` int(10) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_data_karyawan`
--

INSERT INTO `ak_data_karyawan` (`id_karyawan`, `nik`, `nama`, `username`, `password`, `id_level`, `tanggal_lahir`, `alamat`, `no_hp`, `tanggal_dibuat`, `tanggal_dirubah`, `userchange`, `id_outlet`, `id_pengaturan`, `deleted`) VALUES
(1, 141510013, 'SUPERADMIN', 'master', '123', 1, '2019-04-27', 'alamat', '08102884793', '2019-04-01 00:00:00', '2019-03-31 21:08:00', 'SUPERADMIN', 1, 1, 0),
(2, 141510014, 'Kasir', 'kasir', 'kasir', 2, '0000-00-00', 'Alamat', '081289342', '2019-04-07 20:00:32', '2019-04-07 13:00:32', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ak_data_karyawan_level`
--

CREATE TABLE `ak_data_karyawan_level` (
  `id_level` bigint(20) NOT NULL,
  `nama_level` varchar(45) NOT NULL,
  `tanggal_dibuat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_dirubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userchange` text NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_data_karyawan_level`
--

INSERT INTO `ak_data_karyawan_level` (`id_level`, `nama_level`, `tanggal_dibuat`, `tanggal_dirubah`, `userchange`, `deleted`) VALUES
(1, 'SUPERADMIN', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'SUPERADMIN', 0),
(2, 'KASIR', '2019-04-03 10:33:05', '2019-04-03 03:33:05', 'SYSTEM', 0),
(3, 'PEKERJA', '2019-04-03 10:33:05', '2019-04-03 03:33:05', 'SYSTEM', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ak_data_outlet`
--

CREATE TABLE `ak_data_outlet` (
  `id_outlet` int(10) NOT NULL,
  `nama_outlet` varchar(45) NOT NULL,
  `owner_outlet` text NOT NULL,
  `alamat_outlet` text NOT NULL,
  `no_telp_outlet` varchar(35) NOT NULL,
  `no_hp_outlet` varchar(35) NOT NULL,
  `email_outlet` varchar(35) NOT NULL,
  `keterangan_outlet` text NOT NULL,
  `tanggal_dibuat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_dirubah` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userchange` text NOT NULL,
  `id_pengaturan` int(10) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_data_outlet`
--

INSERT INTO `ak_data_outlet` (`id_outlet`, `nama_outlet`, `owner_outlet`, `alamat_outlet`, `no_telp_outlet`, `no_hp_outlet`, `email_outlet`, `keterangan_outlet`, `tanggal_dibuat`, `tanggal_dirubah`, `userchange`, `id_pengaturan`, `deleted`) VALUES
(1, 'Laundry OK', 'Deden Syarif Hidayat', 'Alamat disitu saya senang', '02192189425', '0948392472', 'dedensyarifhidayat@gmail.com', 'Jadikan cuci mu jadi makin ok', '2019-04-07 00:00:00', '2019-04-07 21:52:04', 'SUPERADMIN', 1, 0),
(2, 'Laundry OK 2', 'Deden Syarif Hidayat', 'alamat', '08473829472', '0923847923', 'dedensyarifhidayat@gmail.com', 'tes', '2019-04-07 00:00:00', '2019-04-07 21:56:09', 'SUPERADMIN', 1, 0),
(3, 'Laundry OK 3', 'Deden Syarif Hidayat', 'Jln raya disitu disini disana tes', '082738294723', '047389247239', 'dedensyarifhidayat@gmail.com', '', '2019-04-07 21:57:01', '2019-04-07 21:57:01', 'SUPERADMIN', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ak_data_pengaturan`
--

CREATE TABLE `ak_data_pengaturan` (
  `id_pengaturan` int(10) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `tanggal_dibuat` datetime NOT NULL,
  `tanggal_dirubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tempo` date NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_data_pengaturan`
--

INSERT INTO `ak_data_pengaturan` (`id_pengaturan`, `username`, `password`, `tanggal_dibuat`, `tanggal_dirubah`, `tempo`, `deleted`) VALUES
(1, 'deden', 'deden', '2019-04-07 00:00:00', '2019-04-06 17:00:00', '2019-12-31', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ak_data_produk`
--

CREATE TABLE `ak_data_produk` (
  `id_produk` int(10) NOT NULL,
  `nama_produk` varchar(45) NOT NULL,
  `jenis_produk` text NOT NULL,
  `harga_produk` decimal(22,2) NOT NULL,
  `keterangan_produk` text NOT NULL,
  `tanggal_dibuat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_dirubah` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userchange` text NOT NULL,
  `id_outlet` int(10) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_data_produk`
--

INSERT INTO `ak_data_produk` (`id_produk`, `nama_produk`, `jenis_produk`, `harga_produk`, `keterangan_produk`, `tanggal_dibuat`, `tanggal_dirubah`, `userchange`, `id_outlet`, `deleted`) VALUES
(1, 'Boneka', '2', '8000.00', 'aaaa', '2019-04-07 06:44:00', '2019-04-07 12:29:56', 'SUPERADMIN', 1, 0),
(2, 'Karpet', '3', '45000.00', '', '2019-04-07 19:29:56', '2019-04-07 12:29:56', 'SUPERADMIN', 1, 0),
(3, 'Kiloan', '1', '5000.00', '', '2019-04-07 19:34:28', '2019-04-07 12:34:28', 'SUPERADMIN', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ak_data_customer`
--
ALTER TABLE `ak_data_customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `ak_data_karyawan`
--
ALTER TABLE `ak_data_karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `ak_data_karyawan_level`
--
ALTER TABLE `ak_data_karyawan_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `ak_data_outlet`
--
ALTER TABLE `ak_data_outlet`
  ADD PRIMARY KEY (`id_outlet`);

--
-- Indexes for table `ak_data_pengaturan`
--
ALTER TABLE `ak_data_pengaturan`
  ADD PRIMARY KEY (`id_pengaturan`);

--
-- Indexes for table `ak_data_produk`
--
ALTER TABLE `ak_data_produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ak_data_customer`
--
ALTER TABLE `ak_data_customer`
  MODIFY `id_customer` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ak_data_karyawan`
--
ALTER TABLE `ak_data_karyawan`
  MODIFY `id_karyawan` bigint(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ak_data_karyawan_level`
--
ALTER TABLE `ak_data_karyawan_level`
  MODIFY `id_level` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ak_data_outlet`
--
ALTER TABLE `ak_data_outlet`
  MODIFY `id_outlet` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ak_data_pengaturan`
--
ALTER TABLE `ak_data_pengaturan`
  MODIFY `id_pengaturan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ak_data_produk`
--
ALTER TABLE `ak_data_produk`
  MODIFY `id_produk` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
