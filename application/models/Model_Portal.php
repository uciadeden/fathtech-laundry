<?php
/**
 * 
 */
class Model_Portal extends CI_Model
{
	Protected $karyawan = "ak_data_karyawan";
	Protected $karyawan_level = "ak_data_karyawan_level";
	Protected $outlet = "ak_data_outlet";
	
	function __construct()
	{
		parent::__construct();
	}

	public function proses_login($data){
		$chek_username = $this->db->get_where($this->karyawan,array('username' => $data['username']))->num_rows();

		if($chek_username == 0){
			echo "<script>
			tampil_gagal('Username Tidak Ditemukan !');
			</script>";
			exit();
		}else{

			$chek_password = $this->db->get_where($this->karyawan,array('username' => $data['username'],'password' => $data['password']))->num_rows();

			if($chek_password == 0){
				echo "<script>
				tampil_gagal('Password Salah !');
				</script>";
				exit();
			}else{
				$data_karyawan = $this->db->join($this->karyawan_level.' kl','kl.id_level=k.id_level')->get_where($this->karyawan.' k',array('k.username' => $data['username'],'k.password' => $data['password'],'k.deleted' => FALSE));

				if($data_karyawan->num_rows() == 0){
					echo "<script>
					tampil_gagal('Username ini sudah dihapus / data telah rusak');
					</script>";
					exit();
				}else{

					$data_karyawan = $data_karyawan->row();
					$data_session = array(
						'id_karyawan' => $data_karyawan->id_karyawan,
						'nama_karyawan' => $data_karyawan->nama,
						'login_karyawan' => TRUE,
						'level' => $data_karyawan->nama_level,
						'id_pengaturan' => $data_karyawan->id_pengaturan,
						'id_outlet' => $data_karyawan->id_outlet
					);

					$this->session->set_userdata($data_session);
				}
			}
		}
	}
}

?>