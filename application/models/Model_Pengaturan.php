<?php
/**
 * 
 */
class Model_Pengaturan extends CI_Model
{
	Protected $karyawan = "ak_data_karyawan";
	Protected $karyawan_level = "ak_data_karyawan_level";
	Protected $produk = "ak_data_produk";
	Protected $outlet = "ak_data_outlet";
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_karyawan($id=NULL){
		$this->db->select('*,k.tanggal_dibuat as tanggal,k.tanggal_dirubah as tanggal_edit,k.userchange as userganti');
		$this->db->from($this->karyawan.' k');
		$this->db->join($this->karyawan_level.' kl','k.id_level=kl.id_level');
		$this->db->where('k.deleted',FALSE);
		if($id === NULL){
			return $this->db->get()->result();
		}else{
			$this->db->where('k.id_karyawan',$id);
			return $this->db->get()->row();
		}
	}

	public function simpan_karyawan($data){
		$this->db->trans_begin();
		if($data['id_karyawan'] != ""){
			$chek_nik = $this->db->query("SELECT * FROM $this->karyawan WHERE nik='".$data['nik']."' AND id_karyawan !='".$data['id_karyawan']."' AND id_pengaturan='".$this->session->userdata('id_pengaturan')."'")->num_rows();
			if($chek_nik == 0){
				$chek_username = $this->db->query("SELECT * FROM $this->karyawan WHERE username='".$data['username']."' AND id_karyawan != '".$data['id_karyawan']."'")->num_rows();
				if($chek_username == 0){
					$chek_no_hp = $this->db->query("SELECT * FROM $this->karyawan WHERE no_hp='".$data['no_hp']."' AND id_karyawan != '".$data['id_karyawan']."' AND id_pengaturan='".$this->session->userdata('id_pengaturan')."'")->num_rows();

					if($chek_no_hp == 0){
						$this->db->update($this->karyawan,$data,array('id_karyawan' => $data['id_karyawan']));
					}else{
						echo "<script>
						tampil_gagal('No HP Sudah digunakan');
						</script>";
					}
				}else{
					echo "<script>
					tampil_gagal('Username Sudah Digunakan !');
					</script>";
					exit();
				}
			}else{
				echo "<script>
				tampil_gagal('Nik Sudah Digunakan !');
				</script>";

				exit();
			}
		}else{
			$chek_nik = $this->db->get_where($this->karyawan,array('nik' => $data['nik'],'id_pengaturan' => $this->session->userdata('id_pengaturan')))->num_rows();
			if($chek_nik == 0){

				$chek_username = $this->db->get_where($this->karyawan,array('username' => $data['username']))->num_rows();
				if($chek_username == 0){
					$chek_hp = $this->db->get_where($this->karyawan,array('no_hp' => $data['no_hp'],'id_pengaturan' => $this->session->userdata('id_pengaturan')))->num_rows();
					if($chek_hp == 0){
						$this->db->insert($this->karyawan,$data);
					}else{
						echo "<script>
						tampil_gagal('No HP Sudah Digunakan !');
						</script>";
						exit();
					}
				}else{
					echo "<script>
					tampil_gagal('Username Sudah Digunakan !');
					</script>";
					exit();
				}
			}else{
				echo "<script>
				tampil_gagal('Nik Sudah Digunakan !');
				</script>";
				exit();
			}
		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}

		$x['status'] = $this->db->trans_status();
		$x['error']  = $this->db->error();

		return $x;
	}

	public function hapus_karyawan($data){
		return $this->db->update($this->karyawan,$data,array('id_karyawan' => $data['id_karyawan']));
	}
	public function get_produk($id=NULL){
		$this->db->select('*,p.tanggal_dibuat as tanggal,p.tanggal_dirubah as tanggal_edit,p.userchange as userganti');
		$this->db->from($this->produk.' p');
		$this->db->where('p.deleted',FALSE);
		$this->db->join($this->outlet.' o','p.id_outlet=o.id_outlet');
		if($id === NULL){

			return $this->db->get()->result();
		}else{
			$this->db->where('p.id_produk',$id);

			return $this->db->get()->row();
		}
	}
	public function simpan_produk($data){
		$this->db->trans_begin();
		if($data['id_produk'] != ""){
			$this->db->update($this->produk,$data,array('id_produk' => $data['id_produk']));
		}else{
			$this->db->insert($this->produk,$data);

		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}

		$x['status'] = $this->db->trans_status();
		$x['error']  = $this->db->error();

		return $x;
	}

	public function hapus_produk($data){
		return $this->db->update($this->produk,$data,array('id_produk' => $data['id_produk']));
	}

	public function ganti_outlet($id){
		$this->db->trans_begin();
		$chek = $this->db->where(array('p.id_pengaturan' => $this->session->userdata('id_pengaturan'),'p.id_outlet' => $this->session->userdata('id_outlet')))->get($this->outlet.' p')->num_rows();

		if($chek == 0){
			echo "<script>
			tampil_gagal('Tidak Mempunyai Akses !')
			</script>";
			exit();
		}else{

			$this->session->set_userdata('id_outlet',$id);
		}

		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}

		$x['status'] = $this->db->trans_status();
		$x['error']  = $this->db->error();

		return $x;
	}

	public function get_outlet($id=NULL){
		$this->db->select('*,p.tanggal_dibuat as tanggal,p.tanggal_dirubah as tanggal_edit,p.userchange as userganti');
		$this->db->from($this->outlet.' p');
		$this->db->where('p.deleted',FALSE);
		$this->db->where('p.id_pengaturan',$this->session->userdata('id_pengaturan'));
		if($id === NULL){

			return $this->db->get()->result();
		}else{
			$this->db->where('p.id_outlet',$id);

			return $this->db->get()->row();
		}
	}
	public function simpan_outlet($data){
		$this->db->trans_begin();
		if($data['id_outlet'] != ""){
			$this->db->update($this->outlet,$data,array('id_outlet' => $data['id_outlet']));
		}else{
			$this->db->insert($this->outlet,$data);

		}
		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}

		$x['status'] = $this->db->trans_status();
		$x['error']  = $this->db->error();

		return $x;
	}

	public function hapus_outlet($data){
		return $this->db->update($this->outlet,$data,array('id_outlet' => $data['id_outlet']));
	}
}

?>