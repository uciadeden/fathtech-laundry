<?php
/**
 * 
 */
class Serverside extends CI_Controller
{
	Protected $karyawan_level = "ak_data_karyawan_level";
	function __construct()
	{
		parent::__construct();
	}

	public function serverside_karyawan_level(){
		$cari = $this->input->get('search');
		$result = $this->db->like('nama_level',$cari)->get_where($this->karyawan_level,array('deleted' => FALSE));
		if ($result->num_rows() > 0) {
			$list = array();
			$key=0;
			foreach($result->result_array() as $row) {
				$list[$key]['id'] = $row['id_level'];
				$list[$key]['text'] = $row['nama_level']; 
				$key++;
			}
			echo json_encode($list);
		} else {
			echo "-- HASIL KOSONG --";
		}
	}
}

?>