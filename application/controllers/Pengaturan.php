<?php
/**
 * 
 */
class Pengaturan extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Model_Pengaturan','mp');
		if($this->session->userdata('level') == 'SUPERADMIN'){

		}else{
			redirect(base_url('portal'));
		}
	}
	function tanggal_indo($tanggal, $cetak_hari = false)
	{
		$hari = array ( 1 =>    'Senin',
			'Selasa',
			'Rabu',
			'Kamis',
			'Jumat',
			'Sabtu',
			'Minggu'
		);

		$bulan = array (1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$split 	  = explode('-', date('Y-m-d',strtotime($tanggal)));
		$jam      = date('H:i',strtotime($tanggal));
		$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];

		if ($cetak_hari) {
			$num = date('N', strtotime($tanggal));
			return $hari[$num] . ', ' . $tgl_indo.' '.$jam;
		}
		return $tgl_indo.' '.$jam;
	}

	public function ganti_outlet($id){
		$simpan = $this->mp->ganti_outlet($id);
		$status = $simpan['status'];
		$error  = $simpan['error'];

		if($status === FALSE){
			echo "<script>
			tampil_gagal('$error');
			</script>";
		}else{
			echo "<script>
			tampil_sukses('Halaman akan disegarkan dalam waktu 2 detik');
			</script>";
		}
	}

	public function index(){
		$this->load->view('master/pengaturan/index');
	}

	public function list_data_karyawan() {
		$list = $this->mp->get_karyawan();
		$datatb = array();
		$no = 1;
		foreach($list as $data) {
			$row = array();
			$row[] = '';
			$row[] = $no;
			$row[] = $data->nik;
			$row[] = $data->nama;
			$row[] = $data->nama_level;
			$row[] = $data->username;
			$row[] = $data->password;
			$row[] = $data->no_hp;
			$row[] = $this->tanggal_indo($data->tanggal,FALSE);
			$row[] = $data->userganti;
			$row[] = $this->tanggal_indo($data->tanggal_edit,FALSE);
			$row[] = "<button type='button' onclick='edit(`$data->id_karyawan`)' class='btn btn-sm btn-success'><i class='fa fa-edit'></i></button> 
			<button type='button' id='hapus' onclick='hapus(`$data->id_karyawan`)' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";
			
			$datatb[] = $row;
			$no++;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"data" => $datatb
		);
		echo json_encode($output);
	}

	public function karyawan(){
		$this->load->view('master/pengaturan/v_karyawan');
	}

	public function simpan_karyawan(){
		$data = array();
		$chek = TRUE;

		foreach ($this->input->post() as $key => $value) {
			$data[$key] = $value;

			if($key != "id_karyawan" && $key != "alamat" && $value == ""){
				$chek = FALSE;
			}
		}

		if($chek === FALSE){
			echo "<script>
			tampil_gagal('Data Mohon Diisi Semua !');
			</script>";

			exit();
		}

		$simpan = $this->mp->simpan_karyawan($data);
		$status = $simpan['status'];
		$error  = $simpan['error'];

		if($status === FALSE){
			echo "<script>
			tampil_gagal('$error');
			</script>";
		}else{
			echo "<script>
			tampil_sukses('Data Berhasil Disimpan !');
			oTable.ajax.reload();
			$('#modal_karyawan').modal('hide');
			</script>";
		}
	}

	public function edit_karyawan(){
		$data = $this->mp->get_karyawan($this->input->post('id_karyawan'));

		echo "<script>
		$('#id_karyawan').val('$data->id_karyawan');
		$('#nik').val('$data->nik');
		$('#nama').val('$data->nama');
		$('#username').val('$data->username');
		$('#no_hp').val('$data->no_hp');
		$('#password').val('$data->password');
		$('#alamat').val('$data->alamat');
		$('#id_level').select2('trigger','select',{'data':{id:'$data->id_level',text:'$data->nama_level'}});

		$('#modal_karyawan').modal('show');
		</script>";
	}

	public function hapus_karyawan(){
		$data = array(
			'id_karyawan' => $this->input->post('id_karyawan'),
			'deleted' => TRUE
		);
		$hapus  = $this->mp->hapus_karyawan($data);
		$status = $hapus['status'];
		$error  = $hapus['error'];

		if($status === FALSE){
			echo "<script>
			tampil_gagal('$error');
			</script>";
		}else{
			echo "<script>
			tampil_sukses('Data Berhasil Dihapus !');
			oTable.ajax.reload();
			</script>";
		}
	}

	public function produk(){
		$this->load->view('master/pengaturan/v_produk');
	}

	public function list_data_produk() {
		$list = $this->mp->get_produk();
		$datatb = array();
		$no = 1;
		foreach($list as $data) {
			if($data->jenis_produk == 1){
				$nama_jenis = 'Kiloan';
			}else if($data->jenis_produk == 2){
				$nama_jenis = 'Unit';
			}else if($data->jenis_produk == 3){
				$nama_jenis = 'm2';
			}
			$row = array();
			$row[] = '';
			$row[] = $no;
			$row[] = $data->nama_produk;
			$row[] = $nama_jenis;
			$row[] = $data->harga_produk;
			$row[] = $data->nama_outlet;
			$row[] = $this->tanggal_indo($data->tanggal,FALSE);
			$row[] = $data->userganti;
			$row[] = $this->tanggal_indo($data->tanggal_edit,FALSE);
			$row[] = "<button type='button' onclick='edit(`$data->id_produk`)' class='btn btn-sm btn-success'><i class='fa fa-edit'></i></button> 
			<button type='button' id='hapus' onclick='hapus(`$data->id_produk`)' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";
			
			$datatb[] = $row;
			$no++;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"data" => $datatb
		);
		echo json_encode($output);
	}

	public function simpan_produk(){
		$data = array();
		$chek = TRUE;

		foreach ($this->input->post() as $key => $value) {
			$data[$key] = $value;

			if($key != "id_produk" && $key!= "keterangan_produk" && $value == ""){
				$chek = FALSE;
			}
		}

		if($chek === FALSE){
			echo "<script>
			tampil_gagal('Data Mohon Diisi Semua !');
			</script>";

			exit();
		}

		$data['id_outlet'] = $this->session->userdata('id_outlet');
		$data['userchange'] = $this->session->userdata('nama_karyawan');
		$simpan = $this->mp->simpan_produk($data);
		$status = $simpan['status'];
		$error  = $simpan['error'];

		if($status === FALSE){
			echo "<script>
			tampil_gagal('$error');
			</script>";
		}else{
			echo "<script>
			tampil_sukses('Data Berhasil Disimpan !');
			$('#modal_produk').modal('hide');
			oTable.ajax.reload();
			</script>";
		}
	}

	public function edit_produk(){
		$data = $this->mp->get_produk($this->input->post('id_produk'));

		echo "<script>
		$('#id_produk').val('$data->id_produk');
		$('#nama_produk').val('$data->nama_produk');
		$('#harga_produk').val('$data->harga_produk');
		$('#keterangan_produk').val('$data->keterangan_produk');

		$('#modal_produk').modal('show');
		";

		if($data->jenis_produk == 1){
			echo "$('#jenis_produk_1').attr('checked', 'checked');";
		}else if($data->jenis_produk == 2){
			echo "$('#jenis_produk_2').attr('checked', 'checked');";
		}else if($data->jenis_produk == 3){
			echo "$('#jenis_produk_3').attr('checked', 'checked');";
		}
		echo "</script>";
	}

	public function hapus_produk(){
		$data = array(
			'id_produk' => $this->input->post('id_produk'),
			'deleted' => TRUE
		);
		$hapus  = $this->mp->hapus_produk($data);
		$status = $hapus['status'];
		$error  = $hapus['error'];

		if($status === FALSE){
			echo "<script>
			tampil_gagal('$error');
			</script>";
		}else{
			echo "<script>
			tampil_sukses('Data Berhasil Dihapus !');
			oTable.ajax.reload();
			</script>";
		}
	}

	public function outlet(){
		$this->load->view('master/pengaturan/v_outlet');
	}

	public function list_data_outlet() {
		$list = $this->mp->get_outlet();
		$datatb = array();
		$no = 1;
		foreach($list as $data) {
			$row = array();
			$row[] = '';
			$row[] = $no;
			$row[] = $data->nama_outlet;
			$row[] = $data->owner_outlet;
			$row[] = $data->no_telp_outlet;
			$row[] = $data->no_hp_outlet;
			$row[] = $data->keterangan_outlet;
			$row[] = $data->alamat_outlet;
			$row[] = $this->tanggal_indo($data->tanggal,FALSE);
			$row[] = $data->userganti;
			$row[] = $this->tanggal_indo($data->tanggal_edit,FALSE);
			$row[] = "<button type='button' onclick='edit(`$data->id_outlet`)' class='btn btn-sm btn-success'><i class='fa fa-edit'></i></button> 
			<button type='button' id='hapus' onclick='hapus(`$data->id_outlet`)' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";
			
			$datatb[] = $row;
			$no++;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"data" => $datatb
		);
		echo json_encode($output);
	}

	public function simpan_outlet(){
		$data = array();
		$chek = TRUE;

		foreach ($this->input->post() as $key => $value) {
			$data[$key] = $value;

			if($key != "id_outlet" && $key!= "keterangan_outlet" && $value == ""){
				$chek = FALSE;
			}
		}

		if($chek === FALSE){
			echo "<script>
			tampil_gagal('Data Mohon Diisi Semua !');
			</script>";

			exit();
		}

		$data['userchange'] = $this->session->userdata('nama_karyawan');
		$data['id_pengaturan'] = $this->session->userdata('id_pengaturan');
		$simpan = $this->mp->simpan_outlet($data);
		$status = $simpan['status'];
		$error  = $simpan['error'];

		if($status === FALSE){
			echo "<script>
			tampil_gagal('$error');
			</script>";
		}else{
			echo "<script>
			tampil_sukses('Data Berhasil Disimpan !');
			$('#modal_outlet').modal('hide');
			oTable.ajax.reload();
			</script>";
		}
	}

	public function edit_outlet(){
		$data = $this->mp->get_outlet($this->input->post('id_outlet'));

		echo "<script>
		$('#id_outlet').val('$data->id_outlet');
		$('#nama_outlet').val('$data->nama_outlet');
		$('#owner_outlet').val('$data->owner_outlet');
		$('#no_telp_outlet').val('$data->no_telp_outlet');
		$('#no_hp_outlet').val('$data->no_hp_outlet');
		$('#email_outlet').val('$data->email_outlet');
		$('#keterangan_outlet').val('$data->keterangan_outlet');
		$('#alamat_outlet').val('$data->alamat_outlet');

		$('#modal_outlet').modal('show');
		";
		echo "</script>";
	}

	public function hapus_outlet(){
		$data = array(
			'id_outlet' => $this->input->post('id_outlet'),
			'deleted' => TRUE
		);
		$hapus  = $this->mp->hapus_outlet($data);
		$status = $hapus['status'];
		$error  = $hapus['error'];

		if($status === FALSE){
			echo "<script>
			tampil_gagal('$error');
			</script>";
		}else{
			echo "<script>
			tampil_sukses('Data Berhasil Dihapus !');
			oTable.ajax.reload();
			</script>";
		}
	}
}

?>