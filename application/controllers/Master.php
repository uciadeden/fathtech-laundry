<?php
/**
 * 
 */
class Master extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('level') == 'SUPERADMIN'){

		$this->load->model('Model_Pengaturan','mp');
		}else{
			redirect(base_url('portal'));
		}
	}

	public function index(){
		$data['outlet'] = $this->mp->get_outlet($this->session->userdata('id_outlet'));
		$data['semua_outlet'] = $this->mp->get_outlet();
		$this->load->view('master/index',$data);
	}
}

?>