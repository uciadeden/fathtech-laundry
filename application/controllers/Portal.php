<?php

class Portal extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Model_Portal','mp');
	}

	public function index(){
		$this->load->view('master/portal/index');
	}

	public function proses_login(){
		$data = array();
		$chek = TRUE;

		foreach ($this->input->post() as $key => $value) {
			$data[$key] = $value;

			if($value == ""){
				$chek = FALSE;
			}
		}

		if($chek === FALSE){
			echo "<script>
				tampil_gagal('Data Mohon Diisi Semua !');
			</script>";

			exit();
		}

		$proses = $this->mp->proses_login($data);

		if($proses){
			echo "<script>
				tampil_gagal('Aneh');
			</script>";	
		}else{
			$url = base_url('master');
			echo "<script>
			tampil_sukses('Berhasil Login !','Akan diarahkan dalam waktu 2 detik.');

			setTimeout(() => {
				window.location.href='$url';
				},2000)
			</script>";
		}
	}
}

?>