<!DOCTYPE html>
<html>
<head>
	<title>Master</title>
	<?php $this->load->view('master/pemanis/style'); ?>
</head>
<body>
	<?php $this->load->view('master/pemanis/navbar'); ?>

	<div class="container">
		<div class="row">
			<div class="col-12 mt-2 kerenz">
				<div class="card">
					<div class="card-header">
						<ul class="nav">
							<li class="button-dropdown">
								<a href="javascript:void(0)" class="dropdown-toggle">
									<?= $outlet->nama_outlet; ?>
								</a>
								<ul class="dropdown-menu">
									<?php foreach($semua_outlet as $data_outlet){ ?>
									<li>
										<a href="<?= base_url('Pengaturan/ganti_outlet/').$data_outlet->id_outlet; ?>" class="text-lebih">
											<?= $data_outlet->nama_outlet; ?>
										</a>
									</li>
									<?php } ?>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-4 col-sm-4 col-md-2 kerenz mt-2">
				<div class="card bg-light text-black text-center">
					<div class="card-body">
						<img src="<?= base_url('assets/gambar/business.png') ?>" width='100%'>
					</div>
					<div class="card-footer text-lebih p-2 bg-primary text-white">
						Admin
					</div>
				</div>
			</div>
			<div class="col-4 col-sm-4 col-md-2 kerenz mt-2">
				<div class="card bg-light text-black text-center">
					<div class="card-body">
						<img src="<?= base_url('assets/gambar/business.png') ?>" width='100%'>
					</div>
					<div class="card-footer text-lebih p-2 bg-primary text-white">
						Kasir
					</div>
				</div>
			</div>
			<div class="col-4 col-sm-4 col-md-2 kerenz mt-2">
				<div class="card bg-light text-black text-center">
					<div class="card-body">
						<img src="<?= base_url('assets/gambar/business.png') ?>" width='100%'>
					</div>
					<div class="card-footer text-lebih p-2 bg-primary text-white">
						Pekerja
					</div>
				</div>
			</div>
			<div class="col-4 col-sm-4 col-md-2 kerenz mt-2">
				<a href="<?= base_url('pengaturan/'); ?>">
					<div class="card bg-light text-black text-center">
						<div class="card-body">
							<img src="<?= base_url('assets/gambar/business.png') ?>" width='100%'>
						</div>
						<div class="card-footer text-lebih p-2 bg-primary text-white">
							Pengaturan
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>

	<?php $this->load->view('master/pemanis/javascript'); ?>
	<script type="text/javascript">
		jQuery(document).ready(function (e) {
			function t(t) {
				e(t).bind("click", function (t) {
					t.preventDefault();
					e(this).parent().fadeOut()
				})
			}
			e(".dropdown-toggle").click(function () {
				var t = e(this).parents(".button-dropdown").children(".dropdown-menu").is(":hidden");
				e(".button-dropdown .dropdown-menu").hide();
				e(".button-dropdown .dropdown-toggle").removeClass("active");
				if (t) {
					e(this).parents(".button-dropdown").children(".dropdown-menu").toggle().parents(".button-dropdown").children(".dropdown-toggle").addClass("active")
				}
			});
			e(document).bind("click", function (t) {
				var n = e(t.target);
				if (!n.parents().hasClass("button-dropdown")) e(".button-dropdown .dropdown-menu").hide();
			});
			e(document).bind("click", function (t) {
				var n = e(t.target);
				if (!n.parents().hasClass("button-dropdown")) e(".button-dropdown .dropdown-toggle").removeClass("active");
			})
		});

	</script>
</body>
</html>