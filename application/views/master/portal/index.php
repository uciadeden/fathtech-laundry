<!DOCTYPE html>
<html>
<head>
	<title>Form Masuk</title>
	<?php $this->load->view('master/pemanis/style'); ?>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 mx-auto">
				<div class="card border-primary">
					<div class="card-header bg-primary text-white">
						Form Masuk
					</div>
					<div class="card-body">
						<form id="Frm_Masuk">
							<div class="row">
								<div class="col-12">
									<div class="form-group">
										<label>Username</label>
										<input type="text" class="form-control" name="username">
									</div>
								</div>
								<div class="col-12">
									<div class="form-group">
										<label>Password</label>
										<input type="text" class="form-control" name="password">
									</div>
								</div>
								<div class="col-12">
									<button type="submit" class="btn btn-primary float-right">Masuk</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('master/pemanis/javascript'); ?>
	<script type="text/javascript">
		$("#Frm_Masuk").submit(function(e){
			e.preventDefault();

			$.ajax({
				type:'POST',
				data:$("#Frm_Masuk").serialize(),
				url:'<?= base_url('portal/proses_login') ?>',
				success:function(res){
					$("#divsimpan").html(res);
				}
			})

		})
	</script>
</body>
</html>