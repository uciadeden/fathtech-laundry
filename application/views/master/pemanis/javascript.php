<script type="text/javascript" src="<?= base_url('assets/js/jquery-3.3.1.min.js'); ?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/bootstrap-4.0.0-dist/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/fontawesome-free-5.8.1-web/js/all.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/select2-develop/dist/js/select2.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/toastr-master/build/toastr.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/DataTables/datatables.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/DataTables/DataTables-1.10.18/js/dataTables.bootstrap4.min.js'); ?>"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/js/bootbox.all.min.js'); ?>"></script>

<script type="text/javascript">
	function tampil_sukses($title,$deskripsi){
		toastr.options = {
			'progressBar':true,
			'positionClass':'toast-top-right',
			'timeOut':2000,
			'closeButton':true,
		}

		toastr['success']($deskripsi,$title);
	}
	function tampil_gagal($deskripsi){
		toastr.options = {
			'progressBar':true,
			'positionClass':'toast-top-right',
			'timeOut':5000,
			'closeButton':true,
		}

		toastr['error']($deskripsi,'Mengalami Kesalahan !');
	}
	$(document).ajaxStart(function(){
		$(".loader").show();
	});
	$(document).ajaxComplete(function(){
		$(".loader").hide();
	});
	document.addEventListener("DOMContentLoaded", function() {
		var elements = document.getElementsByTagName("INPUT");
		for (var i = 0; i < elements.length; i++) {
			elements[i].oninvalid = function(e) {
				e.target.setCustomValidity("");
				if (!e.target.validity.valid) {
					e.target.setCustomValidity("Tidak Boleh Kosong !");
				}
			};
			elements[i].oninput = function(e) {
				e.target.setCustomValidity("");
			};
		}
	});
	document.addEventListener("DOMContentLoaded", function() {
		var elements = document.getElementsByTagName("SELECT");
		for (var i = 0; i < elements.length; i++) {
			elements[i].oninvalid = function(e) {
				e.target.setCustomValidity("");
				if (!e.target.validity.valid) {
					e.target.setCustomValidity("Tidak Boleh Kosong !");
				}
			};
			elements[i].oninput = function(e) {
				e.target.setCustomValidity("");
			};
		}
	});
	$(document).on('click', '.bootbox', function (event) {
		bootbox.hideAll()
	});

        var click = function () {
            var divObj = $(this).next();
            var nstyle = divObj.css("display");
            if (nstyle == "none") {
                divObj.slideDown(false, function () {
                    $("html").bind("click", function () {
                        divObj.slideUp();
                    });
                });
            }
        };
        $(".dropmenu").click(click);
</script>