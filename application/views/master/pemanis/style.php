
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/select2-develop/dist/css/select2.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/toastr-master/build/toastr.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/fontawesome-free-5.8.1-web/css/all.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap-4.0.0-dist/css/bootstrap.min.css'); ?>">
<div id="divsimpan"></div>
<meta name="viewport" content="width=device-width, initial-scale=1">
<div class="loader"></div>
<style type="text/css">
	body{
		background-color: #ededed;
		font-size: 90%;
	}

	@media (min-width: 992px) {
		body{
			font-size: 100%;
		} 
	}
	.loader {
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #3498db;
		width: 100px;
		height: 100px;
		-webkit-animation: spin 2s linear infinite; /* Safari */
		animation: spin 2s linear infinite;
		position: absolute;
		top: 50%;
		left: 50%;
		margin: -50px 0px 0px -50px;
		z-index: 1;
		display: none;
	}

	/* Safari */
	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}
	.icon-bar {
		width: 100%; /* Full-width */
		background-color: #0fbcf9; /* Dark-grey background */
		overflow: auto; /* Overflow due to float */
	}

	.icon-bar nav ul ul{

	}

	.icon-bar a {
		float: left; /* Float links side by side */
		text-align: center; /* Center-align text */
		width: 20%; /* Equal width (5 icons with 20% width each = 100%) */
		padding: 12px 0; /* Some top and bottom padding */
		transition: all 0.3s ease; /* Add transition for hover effects */
		color: white; /* White text color */
		font-size: 36px; /* Increased font size */
	}

	.icon-bar a:hover {
		background-color: #34e7e4; /* Add a hover color */
	}
	.kerenz{
		padding: 0px 5px 0px 5px;
	}
	.text-lebih{
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		display: inline-block;
		max-width: 100%;
	}
	.btn{
		border-radius: 0px;
	}
	.toast {
		background-color: none;
	}
	.toast-info {
		background-color: #138496;
	}
	.toast-info2 {
		background-color: #138496;
	}
	.toast-error {
		background-color: #C82333;
	}
	.toast-success {
		background-color: #218838;
	}
	.toast-warning {
		background-color: #E0A800;
	}
	.form-control{
		border-radius: 0px
	}

	.select2-selection__rendered {
		line-height: 31px !important;
	}
	.select2-container .select2-selection--single {
		height: 35px !important;
	}
	.select2-selection__arrow {
		height: 34px !important;
	}
	.bootbox .modal-content{
		margin-top: 50%;
	}
	.modal-content  {
		-webkit-border-radius: 0px !important;
		-moz-border-radius: 0px !important;
		border-radius: 0px !important; 
	}

	.nav {
		display: block;
		margin: 0; 
		padding: 0;
	}

	.nav li {
		display: inline-block;
		list-style: none;
	}

	.nav .button-dropdown {
		position: relative;
	}

	.nav li a {
		display: block;
		color: #333;
		background-color: #fff;
		padding: 10px 20px;
		text-decoration: none;
	}

	.nav li a span {
		display: inline-block;
		margin-left: 5px;
		font-size: 10px;
		color: #999;
	}

	.nav li a:hover, .nav li a.dropdown-toggle.active {
		background-color: #289dcc;
		color: #fff;
	}

	.nav li a:hover span, .nav li a.dropdown-toggle.active span {
		color: #fff;
	}

	.nav li .dropdown-menu {
		display: none;
		position: absolute;
		left: 0;
		padding: 0;
		margin: 0;
		margin-top: 3px;
		text-align: left;
	}

	.nav li .dropdown-menu.active {
		display: block;
	}

	.nav li .dropdown-menu a {
		width: 160px;
	}
</style>