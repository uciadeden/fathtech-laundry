<!DOCTYPE html>
<html>
<head>
	<title>Data Produk !</title>
	<?php $this->load->view('master/pemanis/style'); ?>
</head>
<body>
	<?php $this->load->view('master/pemanis/navbar'); ?>

	<div class="container">
		<div class="row mt-2">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h6>Data Produk
							<div class="float-md-right float-none mt-md-0 mt-2"><button class="btn btn-primary float-right btn-sm btn-block" onclick="tambah()">
								Tambah
							</button>
						</div>
					</h6>
				</div>
				<div class="card-body">
					<table class="table table-sm table-striped table-bordered responsive" id="dtTable" style="width: 100%">
						<thead>
							<tr>
								<th></th>
								<th>No</th>
								<th>Nama</th>
								<th>Jenis</th>
								<th>Harga</th>
								<th>Outlet</th>
								<th>Tanggal Dibuat</th>	
								<th class='none'>Userchange</th>
								<th class='none'>Tanggal Dirubah</th>
								<th class="all">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modal_produk" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">

		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modal Header</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row mt-2">
					<div class="col-12">
						<form id="Frmproduk">
							<div class="row">
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label>Nama Produk</label>
										<input type="number" class="form-control" id="id_produk" name="id_produk" style="display: hidden" hidden readonly>
										<input type="text" class="form-control" id="nama_produk" name="nama_produk" required>
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label>Harga Produk</label>
										<input type="number" class="form-control" id="harga_produk" name="harga_produk" required>
									</div>
								</div>
								<div class="col-12 col-md-12">
									<label>Jenis Produk</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<input name="jenis_produk" id="jenis_produk_1" type="radio" value="1" required>
											</div>
										</div>
										<input type="text" class="form-control" value="Kiloan" readonly>
										<div class="input-group-prepend">
											<div class="input-group-text">
												<input name="jenis_produk" id="jenis_produk_2" type="radio" value="2" required>
											</div>
										</div>
										<input type="text" class="form-control" value="Unit" readonly>
										<div class="input-group-prepend">
											<div class="input-group-text">
												<input name="jenis_produk" id="jenis_produk_3" type="radio" value="3" required>
											</div>
										</div>
										<input type="text" class="form-control" value="m2" readonly>
									</div>
								</div>
								<div class="col-12 col-md-12 mt-3">
									<div class="form-group">
										<label>Keterangan Produk</label>
										<textarea class="form-control" name="keterangan_produk" id="keterangan_produk"></textarea>
									</div>
								</div>
								<div class="col-12 col-md-12">
									<button class="btn btn-primary float-right">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<?php $this->load->view('master/pemanis/javascript'); ?>
<script type="text/javascript">
	function edit($id){
		$.ajax({
			type:'POST',
			data:{id_produk:$id},
			url:'<?= base_url('Pengaturan/edit_produk'); ?>',
			success:function(res){
				$("#divsimpan").html(res);
			}
		})
	}
	function hapus($id){
		bootbox.dialog({
			message:"Apakah kamu yakin ?",
			backdrop: true,
			buttons: {
				yes: { 
					label: "Ya Hapus Sekarang !",
					className: "btn-primary text-white",
					callback: function(result) { 
						$.ajax({
							type:'POST',
							data:{id_produk:$id},
							url:'<?= base_url('Pengaturan/hapus_produk') ?>',
							success:function(res){
								$("#divsimpan").html(res);
							}
						})
					}
				},
				no: { 
					label: "Batalkan",
					className: "btn-secondary",
					callback: function(result) {
					}
				}
			}
		}); 
	}

	function tambah(){
		$("#Frmproduk").trigger('reset');
		$('input[name=jenis_produk]').attr('checked',false);
		$('#id_level').html('');
		$("#modal_produk").modal('show');
	}
	$("#Frmproduk").submit(function(e){
		e.preventDefault();

		$.ajax({
			type:'POST',
			data:$("#Frmproduk").serialize(),
			url:'<?= base_url('Pengaturan/simpan_produk'); ?>',
			success:function(res){
				$("#divsimpan").html(res);
			}
		})
	})
	var oTable = $('#dtTable').DataTable({ 

		"processing": true, 
		"order": [], 

		"ajax": {
			"url": "<?php echo base_url('pengaturan/list_data_produk')?>",
			"type": "POST"
		},


		"columnDefs": [
		{ 
			"targets": [ 0 ], 
			"orderable": false, 
		},
		],

	});
</script>
</body>
</html>