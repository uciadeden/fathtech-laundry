<!DOCTYPE html>
<html>
<head>
	<title>Data Outlet !</title>
	<?php $this->load->view('master/pemanis/style'); ?>
</head>
<body>
	<?php $this->load->view('master/pemanis/navbar'); ?>

	<div class="container">

		<div class="row mt-2">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<h6>Data Outlet
							<div class="float-md-right float-none mt-md-0 mt-2"><button class="btn btn-primary float-right btn-sm btn-block" onclick="tambah()">
								Tambah
							</button>
						</div>
					</h6>
				</div>
				<div class="card-body">
					<table class="table table-sm table-striped table-bordered responsive" id="dtTable" style="width: 100%">
						<thead>
							<tr>
								<th></th>
								<th>No</th>
								<th>Nama Outlet</th>
								<th>Owner</th>
								<th>No Telpon</th>
								<th>No HP</th>
								<th class='none'>Keterangan</th>
								<th class='none'>Alamat</th>
								<th>Tanggal Dibuat</th>
								<th class='none'>Userchange</th>
								<th class='none'>Tanggal Dirubah</th>
								<th class="all">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modal_outlet" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered">

		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modal Header</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row mt-2">
					<div class="col-12">
						<form id="Frmoutlet">
							<div class="row">
								<div class="col-12 col-md-4">
									<div class="form-group">
										<label>Nama Outlet</label>
										<input type="number" class="form-control" id="id_outlet" name="id_outlet" style="display: hidden" hidden readonly>
										<input type="text" class="form-control" id="nama_outlet" name="nama_outlet" required>
									</div>
								</div>
								<div class="col-12 col-md-4">
									<div class="form-group">
										<label>Owner</label>
										<input type="text" class="form-control" id="owner_outlet" name="owner_outlet" required>
									</div>
								</div>
								<div class="col-12 col-md-4">
									<div class="form-group">
										<label>No Telpon</label>
										<input type="text" class="form-control" id="no_telp_outlet" name="no_telp_outlet" required>
									</div>
								</div>
								<div class="col-12 col-md-4">
									<div class="form-group">
										<label>No HP</label>
										<input type="text" class="form-control" id="no_hp_outlet" name="no_hp_outlet" required>
									</div>
								</div>
								<div class="col-12 col-md-4">
									<div class="form-group">
										<label>Email</label>
										<input type="email" class="form-control" id="email_outlet" name="email_outlet" required>
									</div>
								</div>
								<div class="col-12 col-md-12">
									<div class="form-group">
										<label>Keterangan</label>
										<textarea class="form-control" name="keterangan_outlet" id="keterangan_outlet"></textarea>
									</div>
								</div>
								<div class="col-12 col-md-12">
									<div class="form-group">
										<label>Alamat</label>
										<textarea class="form-control" name="alamat_outlet" id="alamat_outlet" required></textarea>
									</div>
								</div>
								<div class="col-12 col-md-12">
									<button class="btn btn-primary float-right">Simpan</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<?php $this->load->view('master/pemanis/javascript'); ?>
<script type="text/javascript">
	function edit($id){
		$.ajax({
			type:'POST',
			data:{id_outlet:$id},
			url:'<?= base_url('Pengaturan/edit_outlet'); ?>',
			success:function(res){
				$("#divsimpan").html(res);
			}
		})
	}
	function hapus($id){
		bootbox.dialog({
			message:"Apakah kamu yakin ?",
			backdrop: true,
			buttons: {
				yes: { 
					label: "Ya Hapus Sekarang !",
					className: "btn-primary text-white",
					callback: function(result) { 
						$.ajax({
							type:'POST',
							data:{id_outlet:$id},
							url:'<?= base_url('Pengaturan/hapus_outlet') ?>',
							success:function(res){
								$("#divsimpan").html(res);
							}
						})
					}
				},
				no: { 
					label: "Batalkan",
					className: "btn-secondary",
					callback: function(result) {
					}
				}
			}
		}); 
	}

	function tambah(){
		$("#Frmoutlet").trigger('reset');
		$('#id_level').html('');
		$("#modal_outlet").modal('show');
	}
	$('#id_level').select2({
		placeholder: '-- MASUKAN LEVEL --',
		ajax: {
			dataType: 'json',	
			url: '<?= base_url('Serverside/serverside_outlet_level'); ?>',
			delay: 0,
			data: function(params) {
				return {
					search: params.term
				}
			},
			processResults: function (data, page) {
				return {
					results: data
				};
			},
		}
	});

	$('select').on('select2:open', function() {
		$('.select2-search input').prop('focus', 0);
	});
	$("#Frmoutlet").submit(function(e){
		e.preventDefault();

		$.ajax({
			type:'POST',
			data:$("#Frmoutlet").serialize(),
			url:'<?= base_url('Pengaturan/simpan_outlet'); ?>',
			success:function(res){
				$("#divsimpan").html(res);
			}
		})
	})
	var oTable = $('#dtTable').DataTable({ 
        "responsive": true,
		"processing": true, 
		"order": [], 

		"ajax": {
			"url": "<?php echo base_url('pengaturan/list_data_outlet')?>",
			"type": "POST"
		},


		"columnDefs": [
		{ 
			"targets": [ 0 ], 
			"orderable": false, 
		},
		],

	});
</script>
</body>
</html>