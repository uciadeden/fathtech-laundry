<!DOCTYPE html>
<html>
<head>
	<title>Pengaturan !</title>
	<?php $this->load->view('master/pemanis/style'); ?>
</head>
<body>
	<?php $this->load->view('master/pemanis/navbar'); ?>

	<div class="container">
		<div class="row">
			<div class="col-4 col-sm-4 col-md-2 kerenz mt-2">
				<a href="<?= base_url('pengaturan/karyawan'); ?>">
					<div class="card bg-light text-black text-center">
						<div class="card-body">
							<img src="<?= base_url('assets/gambar/business.png') ?>" width='100%'>
						</div>
						<div class="card-footer text-lebih p-2 bg-primary text-white">
							Data Karyawan
						</div>
					</div>
				</a>
			</div>
			<div class="col-4 col-sm-4 col-md-2 kerenz mt-2">
				<a href="<?= base_url('pengaturan/produk'); ?>">
					<div class="card bg-light text-black text-center">
						<div class="card-body">
							<img src="<?= base_url('assets/gambar/business.png') ?>" width='100%'>
						</div>
						<div class="card-footer text-lebih p-2 bg-primary text-white">
							Data Produk
						</div>
					</div>
				</a>
			</div>
			<div class="col-4 col-sm-4 col-md-2 kerenz mt-2">
				<a href="<?= base_url('pengaturan/outlet'); ?>">
					<div class="card bg-light text-black text-center">
						<div class="card-body">
							<img src="<?= base_url('assets/gambar/business.png') ?>" width='100%'>
						</div>
						<div class="card-footer text-lebih p-2 bg-primary text-white">
							Outlet
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
	<?php $this->load->view('master/pemanis/javascript'); ?>
</body>
</html>